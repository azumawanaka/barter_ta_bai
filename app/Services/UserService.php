<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Repositories\UserRepository;

/**
 * Class UserService.
 */
class UserService
{
    public static $updateExceptProperties = [
        'email'
    ];

    /**
     * UserService constructor.
     * 
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function existsValidEmail($email) 
    {
        return $this->userRepository->existsValidEmail($email);
	}

}
