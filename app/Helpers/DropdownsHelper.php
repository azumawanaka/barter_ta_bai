<?php

namespace App\Helpers;

use App\Models\Item;

/**
 * Class DropdownsHelper
 */ 
class DropdownsHelper
{
    /**
     * @return Array
     **/
    public static function selectItemCategory()
    {
        $opt = '<option disabled selected>Select Category</option>';
        foreach (Item::$itemCategories as $key => $value) {
            $opt .= '<option value="'.$key.'">'.$value.'</option>';
        }
        return $opt;
    }
    
}
