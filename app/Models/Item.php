<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;
    
    protected $table = 'items';

    protected $fillable = [
        'user_id',
    ];

    /** @var array */
    public static $itemCategories = [
        1 => 'Phones',
        2 => 'Laptops',
        3 => 'Guitars',
        4 => 'Bags',
        5 => 'T-shirts',
        6 => 'Shorts',
        7 => 'Underwears',
        8 => 'Shoes',
        9 => 'Fans',
        10 => 'Refrigerators',
        11 => 'Airconditions',
        12 => 'Watches',
        13 => 'Speakers',
        14 => 'Bikes',
        15 => 'Others'
    ];
}
