<?php

namespace App\Repositories;

use App\Models\User;

/**
 * Class UserRepository
 * @package App\Repositories
 */
class UserRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return User::class;
    }

     /**
     * Get user by email
     *
     * @param $email
     *
     * @return User
     */
    public function findEmail($email)
    {
        $query = $this->model->where([ 'email' => $email])->first();
        // if(count($query) < 1) {
        //     return response('Email not found', 200)
        //           ->header('Content-Type', 'text/plain');
        // }
        return $query;
    }
}
