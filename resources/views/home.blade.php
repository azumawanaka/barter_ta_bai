@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="card shadow border-0 col-md-12 px-0">
            <form action="" method="" class="card-body border-bottom">
                @csrf
                <div class="input-group">
                    <input type="text" name="keyword" class="form-control search-field border-0" placeholder="@lang('labels.search_item')">
                    <div class="input-group-append">
                        <button class="btn btn-info text-white px-4" type="submit"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </form>
            <div class="row p-4">
                <div class="col-md-3">
                    <div class="left-box">
                        <form action="">
                            <div class="form-group">
                                <label for="item-category" class="text-muted">@lang('labels.item_category')</label>
                                <select class="form-control" id="item-category">
                                    {!! DropdownsHelper::selectItemCategory() !!}
                                </select>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="border-left col-md-9 col-xs-12">
                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatibus dolor pariatur tempore dicta, error libero cumque fugit, atque eveniet vitae quaerat quisquam optio modi praesentium est, dolorem temporibus. Ratione, error?
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
