@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="card">
                <div class="card-body">
                    <form role="form" method="POST" action="{{ route('register') }}"  data-toggle="validator">
                        @csrf

                        <div class="form-group row has-feedback">
                            <label for="name" class="col-md-12 col-form-label control-label">{{ __('Full Name') }}</label>
                            <div class="col-md-12">
                                <input 
                                    id="name"
                                    type="text"
                                    class="form-control"
                                    name="name"
                                    value="{{ old('name') }}"
                                    data-name
                                    autocomplete="name"
                                    required
                                    autofocus>
                                <div class="help-block with-errors text-danger"></div>
                            </div>
                        </div>

                        <div class="form-group row has-feedback">
                            <label for="email" class="col-md-12 col-form-label control-label">{{ __('E-Mail Address') }}</label>
                            <div class="col-md-12">
                                <input
                                    id="email"
                                    type="email"
                                    class="form-control"
                                    name="email"
                                    data-email
                                    value="{{ old('email') }}"
                                    data-remote=""
                                    data-remote-error="{{ trans('validation.unique', ['attribute' => __('E-Mail Address')]) }}"
                                    required
                                    autocomplete="email">

                                <div class="help-block with-errors text-danger"></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-12 col-form-label control-label">{{ __('Password') }}</label>
                            <div class="col-md-12">
                                <input
                                    id="password"
                                    type="password"
                                    class="form-control"
                                    name="password"
                                    data-minlength="6"
                                    data-password
                                    data-required-error="{{ trans('validation.required', ['attribute' => 'Password']) }}"
                                    data-minlength-error="{{ trans('validation.custom.minlength', ['attribute' => __('Password'), 'minlength' => 6]) }}"
                                    required
                                    autocomplete="password">

                                <div class="help-block with-errors text-danger"></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-12 col-form-label control-label">{{ __('Confirm Password') }}</label>
                            <div class="col-md-12">
                                <input
                                    id="password-confirm"
                                    type="password"
                                    class="form-control"
                                    name="password_confirmation"
                                    data-match="#password"
                                    data-match-error="{{ trans('validation.custom.notmatch', ['attribute' => __('Password')]) }}"
                                    required
                                    autocomplete="password_confirmation">

                                <div class="help-block with-errors text-danger"></div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
