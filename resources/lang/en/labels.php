<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    */
    'search_item' => 'Search for an item',
    'item_category'    => 'ITEM CATEGORY',
];
